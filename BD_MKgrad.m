function [Aout, Xout, stats] = BD_MKgrad(Y, Ain, Xin, lambda, zeta, mu, varargin)
    addpath ./helpers
    figure (1);
    m = size(Y);
    k = size(Ain);
    
    if (numel(k) >= 3)
        N = k(3);
        k = k(1:2);
        m = m(1:2);
    else
        N = 1;
    end
    %% Options for Grad-Decent solver
    MAXIT = 100;
    EPSILON = 1e-2;
    EPSILON_A = 1e-5;
    EPSILON_GRAD = 1e-4;
    INVTOL = 1e-6;
    INVIT = 2e2;
    C1 = 2e-1; %the rate of decrease of alpha
    ALPHATOL = 1e-10;
    store = 0;
    stats = 0;
    %% Handle extra variables
    nvarargin = numel(varargin);
    if nvarargin > 4
        error('Too many input arguments.');
    end
    
%     idx = 1;
%     if nvarargin < idx || isempty(varargin{idx})
%         Xinit = randn([m, N]);%xsolver_mk_pdNCG(Y, Ain, lambda, mu, [], INVTOL, INVIT);
%         
%     else
%         Xinit = varargin{idx};
%     end
    
    idx = 1;
    if nvarargin < idx || isempty(varargin{idx})
        dispfun = @(a,x) 0;
    else
        dispfun = varargin{idx};
    end
    
%     idx = 3;
%     if nvarargin < idx || isempty(varargin{idx}) || strcmp(varargin{idx}, 'TR')
%         ManoptSolver = @trustregions;
%     elseif strcmp(varargin{idx}, 'SD')
%         ManoptSolver = @steepestdescent;
%     elseif strcmp(varargin{idx}, 'CG')
%         ManoptSolver = @conjugategradient;
%     else
%         error('Invalid solver option.')
%     end
    
    idx = 2;
    if nvarargin >= idx && ~isempty(varargin{idx})
        MAXIT = varargin{idx};
    end
    
    
    %% set the problem
    suppack.Y = Y;
    suppack.k = k;
    suppack.m = m;
    suppack.N = N;
    suppack.lambda = lambda;
    suppack.zeta = zeta;
    suppack.mu = mu;
    suppack.INVTOL = INVTOL;
    suppack.INVIT = INVIT;
     
%     problem.cost = @(a,x,store) costfun(a, x, store,suppack);
%     problem.egrada = @(a,x,store) egradafun(a, x, store, suppack);
%     problem.egradx = @(a,x,store) egradxfun(a, x, store, suppack);
    
    %% run the solver
    doagain = true; it = 0;
    X = Xin;
    A = Ain;
    A_now = A;
    %alpha_X = 1/C1;
    %alpha_A = 1/C1;
    while doagain
        it = it + 1;
        % do gradient min for X
        doagain_X = true; it_X = 0;
        X_minus2 = X; X_minus1 = X; X_now = X;
        alpha_X = 1;
        phi = costfun(A, X, dispfun, suppack);
        while doagain_X
            it_X = it_X + 1; disp(strcat('run X ', num2str(it_X)));
            Y_x = X_minus1 + (it_X-2)/(it_X+1) * (X_minus1 - X_minus2);
            [egradx_Y, store] = egradxfun(A, Y_x, store, suppack);
            X_new = Y_x - alpha_X * egradx_Y;
            %phi_old = costfun(A, Y, dispfun, suppack);
            phi_new = costfun(A, X_new, dispfun,suppack);
            %backtrack search for alpha
            alpha_xtoolow = false;
            while phi_new > phi + sum(sum(egradx_Y .* (X_new - Y_x))) + norm(vec(X_new - Y_x))^2/(2*alpha_X)
                alpha_X = C1 * alpha_X;
                disp('strink alpha_X');
                X_new = Y_x - alpha_X * egradx_Y;
                phi_new = costfun(A, X_new, dispfun, suppack);
                alpha_xtoolow = alpha_X < ALPHATOL;
            end
            %check the condition for iterations
            if ~alpha_xtoolow 
                X_minus2 = X_minus1;
                X_minus1 = X_now;
                X_now = X_new;
                phi = phi_new;
                stats = statsfun(A, X_now, store, stats, dispfun, suppack);
            end
            [egradx_now store] = egradxfun(A, X_now, store, suppack);
            doagain_X = norm(egradx_now(:)) > EPSILON && ~alpha_xtoolow && (it_X < MAXIT);
        end
        phi_X = phi; %cache for results of X iteration
        %do the grad decent on A
        doagain_A = true; it_A = 0;
        A_minus2 = A; A_minus1 = A; A_now = A;
        alpha_A = 1;%fix step size
        phi = costfun(A, X_now, dispfun, suppack);
        while doagain_A
            it_A = it_A + 1;
            disp(strcat('run A ', num2str(it_A)));
            Y_a = A_minus1 +(it_A - 2)/(it_A+1) * (A_minus1 - A_minus2);
            [egrada_Y, store] = egradafun(Y_a, X_now, store, suppack);
            A_new = Y_a - alpha_A * egrada_Y;
            phi_new = costfun(Y_a, X_now, dispfun, suppack);
            %use backtrack to search alpha
            alpha_atoolow = false;
            %phi_new = costfun(A_new, X_now, dispfun, suppack);
%             for i = 1:N % project A back to the sphere
%                 tmp = A_new(:,:,i);
%                 A_new(:,:,i) = A_new(:,:,i)/norm(tmp(:));
%             end
            while phi_new > phi + sum(sum(egrada_Y .* (A_new - Y_a))) + norm(vec(A_new - Y_a))/(2*alpha_A)
                alpha_A = C1*alpha_A;
                A_new = Y_a - alpha_A * egrada_Y;
                phi_new = costfun(A_new, X_now, dispfun, suppack);
                alpha_atoolow = alpha_A < ALPHATOL;
            end
            %check the condition for iteration
            if ~alpha_atoolow 
                A_minus2 = A_minus1;
                A_minus1 = A_now;
                A_now = A_new;
                phi = phi_new;
                stats = statsfun(A_now, X_now, stats, store, dispfun, suppack);
            end
            egrada_now = egradafun(A_now, X_now, store, suppack);
            doagain_A = norm(egrada_now(:)) > EPSILON_A && ~alpha_atoolow && (it_A < MAXIT);
        end
        egrada = egradafun(A_now, X_now, store, suppack);
        egradx = egradxfun(A_now, X_now, store, suppack);
        doagain = ((norm(egrada(:)) + norm(egradx(:))) > EPSILON_GRAD) && it < MAXIT;
        if doagain
            X = X_now;
            A = A_now;
        end
    end
    Aout = A;
    Xout = X;
    stats.f = phi;
    stats.it = it;
    stats.norm = norm(egrada(:)) + norm(egradx(:));
end

function [stats] = statsfun(A,X,stats,store,dispfun,suppack)
    k = suppack.k;
    N = suppack.N;
    dispfun(A(:,:,1),X(:,:,1));
    pause(0.1);
end

function [cost, store] = costfun(A,X,store, suppack)
    N = suppack.N;
    k = suppack.k;
    m = suppack.m;
    Y = suppack.Y;
    lambda = suppack.lambda;
    mu = suppack.mu;
    zeta = suppack.zeta;
    
    tmp = zeros(m);
    pHuber = 0;
    mag_a = 0;
    
    for i = 1:N
        tmp = tmp + cconvfft2(A(:,:,i), X(:,:,i));
        pHuber = pHuber + sum(sum(sqrt(mu^2 + X(:,:,i).^2) - mu));
        mag_a = mag_a + norm(A(:,:,i),'fro');
    end
    
    cost = norm(tmp - Y, 'fro')^2/2 + lambda * pHuber + zeta * mag_a;
end

function [egrada, store] = egradafun(A, X, store, suppack) 
    N = suppack.N;
    k = suppack.k;
    m = suppack.m;
    Y = suppack.Y;
    zeta = suppack.zeta;
    egrada = zeros([k,N]);
    
    tmp = zeros(m);
    for i = 1:N
        tmp = tmp + cconvfft2(X(:,:,i), A(:,:,i));
    end
    tmp = tmp - Y;
    
    for i = 1:N
        tmp_grad = cconvfft2(X(:,:,i),tmp,m,'left');
        tmp_grad = tmp_grad(1:k(1),1:k(2));
        egrada(:,:,i) = tmp_grad + zeta * A(:,:,i);%the size of grada is exactly same as A ([k N])
    end
end

function [egradx, store] = egradxfun(A, X, store, suppack)
    N = suppack.N;
    k = suppack.k;
    m = suppack.m;
    Y = suppack.Y;
    lambda = suppack.lambda;
    mu = suppack.mu;
    
    tmp_gx = zeros([m, N]);
    tmp = zeros(m);
    for i = 1:N
        tmp = tmp + cconvfft2(A(:,:,i),X(:,:,i));
    end
    for i = 1:N
        tmp_gx(:,:,i) = cconvfft2(A(:,:,i), tmp - Y, m, 'left') + lambda * X(:,:,i)./sqrt(mu^2+X(:,:,i).^2);
    end
    
    egradx = tmp_gx; %the size of gradx is same as X ([m N])
end

