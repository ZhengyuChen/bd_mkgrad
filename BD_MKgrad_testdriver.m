clear; clc; clf;
addpath('./helpers');

%% *EITHER, generate {Ai} randomly:

k = [10 10];        % size of A
N = 1;              % number of kernels

A0 = randn([k N]);
for i = 1:N
    tmp = A0(:,:,i);
    A0(:,:,i) = A0(:,:,i)/norm(tmp(:));
end

%% *Parameters to play around with:
m       = [50 50];    % size of x0 and Y
theta   = 1e-3;         % sparsity
eta     = 1e-3;         % additive Gaussian noise variance

%% Generate Y
X0 = zeros([m,N]);
Y = zeros(m);
for i = 1:N
    X0(:,:,i) = double(rand(m) <= theta);
    Y = Y + cconvfft2(A0(:,:,i), X0(:,:,i));
end
%Y = Y + sqrt(eta)*randn([m N]);     % add noise

%% default for the options
mu = 1e-6;              % Approximation quality of the sparsity promoter.
maxit = 100;
kplus = ceil(0.5*k);    % For Phase II/III: k2 = k + 2*kplus.
dispfun = ...           % the interface is a little wonky at the moment
    @( Y, a, X, k, kplus, idx ) showims( Y, A0(:,:,1), X0(:,:,1), a, X, kplus, 1 );
dispfun1 = @(a, X) dispfun(Y, a, X, k, [], 1);

%% run the MKgrad
Ain = randn([k N]); %Ain = Ain/norm(Ain(:));
for i = 1:N
    tmp = Ain(:,:,i);
    Ain(:,:,i) = Ain(:,:,i)/norm(tmp(:));
end
Xin = zeros([m,N]); %Xin = Xin/norm(Xin(:));
lambda = 0.05;%control the sparsity
zeta = 0.01;%control the magnituide of A
[Aout, Xout, stats] = BD_MKgrad(Y, Ain, Xin, lambda, zeta, mu, dispfun1, maxit);
